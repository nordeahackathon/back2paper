# Back2Paper

take a picture of a note, send it as a email to Back2Paper, and it will try to create a incident in remedy based on you note.

## Prerequisite

Remedy integration service running at: https://spoc.test.itcm.oneadr.net/

## Build

## Testing

  curl https://spoc.test.itcm.oneadr.net/incident/INC000081624613

  curl -H "Content-Type: application/json" -X POST -d '{"firstname": "Johannes Hans P Skov","lastname":"Frandsen","summary":"summary"}' https://spoc.test.itcm.oneadr.net/incident

For now, only the summary field can be changed.

You will revice a json response on success with the incident id like this:

  {
      "Incident_Number": "INC000081624613"
  }

You can log in here to see the incident in Remedy: http://cma1cs0415.qaoneadr.local:8020 (development server)
