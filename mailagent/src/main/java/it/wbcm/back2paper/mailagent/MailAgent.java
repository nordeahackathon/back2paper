package it.wbcm.back2paper.mailagent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailAgent implements CommandLineRunner {
	
	@Autowired private ExchangeReader reader;

	public static void main(String[] args) {
        SpringApplication.run(MailAgent.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		if (reader.start()) {
			Thread.currentThread().join();
		}
	}

}
