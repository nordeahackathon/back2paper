package it.wbcm.back2paper.mailagent;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.PropertySet;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.notification.EventType;
import microsoft.exchange.webservices.data.core.enumeration.property.WellKnownFolderName;
import microsoft.exchange.webservices.data.core.enumeration.service.DeleteMode;
import microsoft.exchange.webservices.data.core.exception.service.local.ServiceLocalException;
import microsoft.exchange.webservices.data.core.service.folder.Folder;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.core.service.item.Item;
import microsoft.exchange.webservices.data.core.service.schema.EmailMessageSchema;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.misc.AsyncCallbackImplementation;
import microsoft.exchange.webservices.data.misc.IAsyncResult;
import microsoft.exchange.webservices.data.notification.NotificationEventArgs;
import microsoft.exchange.webservices.data.notification.StreamingSubscription;
import microsoft.exchange.webservices.data.notification.StreamingSubscriptionConnection;
import microsoft.exchange.webservices.data.notification.StreamingSubscriptionConnection.INotificationEventDelegate;
import microsoft.exchange.webservices.data.property.complex.Attachment;
import microsoft.exchange.webservices.data.property.complex.AttachmentCollection;
import microsoft.exchange.webservices.data.property.complex.FileAttachment;
import microsoft.exchange.webservices.data.property.complex.FolderId;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import microsoft.exchange.webservices.data.search.FindItemsResults;
import microsoft.exchange.webservices.data.search.ItemView;
import microsoft.exchange.webservices.data.search.filter.SearchFilter;

@Component
public class ExchangeReader implements INotificationEventDelegate {
	private static final Logger LOG = LoggerFactory.getLogger(ExchangeReader.class);
	
	@Autowired
	private ExchangeConfiguration configuration;
	
	
	private ExchangeService service;
	private Folder folder;
	private StreamingSubscriptionConnection connection;

	public boolean start() {
		LOG.info("Configuration: " + configuration);
		service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
		ExchangeCredentials credentials = new WebCredentials(configuration.getUsername(), configuration.getPassword());
		service.setCredentials(credentials);
		
		try {
			service.setUrl(new URI("https://" + configuration.getServer() + "/EWS/Exchange.asmx"));
		} catch (URISyntaxException e) {
			LOG.error("Something is wrong with the Exchange server URL", e);
			service.close();
			return false;
		}

		try {
			folder = Folder.bind(service, WellKnownFolderName.Inbox);
		} catch (Exception e) {
			LOG.error("Wah! Couldn't bind to the Inbox folder", e);
			return false;
		}

		LOG.info("Logged in and bound to the inbox!");

		try {
			subscribeForStreamingNotifications(service);
		} catch (Exception e) {
			LOG.error("Exception trying to subscribe", e);
			if (connection != null) {
				connection.close();
			}
			return false;
		}
		
		LOG.info("Subscribed to streaming notifications!");
		
		return true;
	}
	
	private void subscribeForStreamingNotifications(ExchangeService service) throws Exception {
		FolderId id = new FolderId(WellKnownFolderName.Inbox);
		ArrayList<FolderId> ids = new ArrayList<>();
		ids.add(id);
		IAsyncResult asyncResult = service.beginSubscribeToStreamingNotifications(new AsyncCallbackImplementation(), null, ids, EventType.NewMail);
		StreamingSubscription subscription = service.endSubscribeToStreamingNotifications(asyncResult);
		connection = new StreamingSubscriptionConnection(service, 30); //TODO 30 minutes
		connection.addSubscription(subscription);
		connection.addOnNotificationEvent(this);
		connection.open();
	}

	@Override
	public void notificationEventDelegate(Object sender, NotificationEventArgs args) {
		LOG.info("notificationEventDelegate called - checking for unread mail");
		processMailItems();
	}

	private void processMailItems() {
		ItemView view = new ItemView(10);
		SearchFilter filter = new SearchFilter.IsEqualTo(EmailMessageSchema.IsRead, false);
		try {
			FindItemsResults<Item> results = service.findItems(folder.getId(), filter, view);
			LOG.info("processMailItems got " + results.getTotalCount() + " items");
			
			service.loadPropertiesForItems(results, PropertySet.FirstClassProperties);
			for (Item item : results.getItems()) {
				processItem(item);
			}
		} catch (ServiceLocalException e1) {
			LOG.error("ServiceLocalException encountered when trying to read mail", e1);
		} catch (Exception e1) {
			LOG.error("Exception encountered when trying to read mail", e1);
		}
	}

	private void processItem(Item item) throws ServiceLocalException, Exception {
		LOG.debug("Processing an item with id " + item.getId() + " and subject " + item.getSubject());
		if (item.getSubject().contains("#b2p") && !item.getSubject().startsWith("RE:")) {
			LOG.info("Yay! It's a Back2Paper request!");
			boolean iDidSomething = false;
			AttachmentCollection collection = item.getAttachments();
			if (collection.getCount() == 0) {
				LOG.error("Expected to get some attachments - didn't find any");
				replyTo(item, "Sorry, I didn't see an attachment on that message.  Please send me an image of your request.");
				iDidSomething = true;
			}
			List<Attachment> attachments = collection.getItems();
			for (Attachment attachment : attachments) {
				LOG.info("Found an attachment with name " + attachment.getName() + " and type " + attachment.getContentType());
				if (attachment instanceof FileAttachment) {
					LOG.info("Attachment is a file attachment");
					FileAttachment fileAttachment = (FileAttachment) attachment;
					fileAttachment.load();
					//TODO do something with the attachment
					
					replyTo(item, "Hi, your message was received!  Eventually someone will implement some processing so an incident is created.");
					iDidSomething = true;
				}
				
			}
			if (iDidSomething) {
				item.delete(DeleteMode.MoveToDeletedItems);
			}
		}
	}
	
	private void replyTo(Item item, String message) {
		if (item instanceof EmailMessage) {
			EmailMessage emailMessage = (EmailMessage)item;
			MessageBody body = new MessageBody(message);
			try {
				emailMessage.reply(body, true);
			} catch (Exception e) {
				LOG.error("Failed to reply to the message", e);
			}
			
		}
	}

}
