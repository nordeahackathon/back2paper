To run this:

1. follow the instructions at:
https://confluence.atlassian.com/kb/connecting-to-ssl-services-802171215.html#ConnectingtoSSLservices-portecleUsingPortecle
to add the SSL stuff for exch-eas.oneadr.net to your keystore

2. add the following lines to application.properties:

exchange.server=exch-eas.oneadr.net
exchange.username=gXXXXX
exchange.password=[your password]

